from sklearn.neural_network import MLPRegressor
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from random import random
import numpy as np
import matplotlib.pyplot as plt
from math import *
import time

start = time.time()

X = [] #holds the data we extract from the projections, the mean and stddev
y = [] #holds the truth level meanx, meany, covariance matrix elements and the rotation angle
ndata = 50000
for i in range(ndata):
    if(i%int(0.1*ndata)==0):
        print(100*i/ndata,"% done")
    dxx = random()*2
    dyy = random()*2
    dxy = random()*2
    while (dxy > dxx or dxy > dyy):
        dxy = random()*2
    meanx = -5+random()*10
    meany = -5+random()*10
    #meanx = 0
    #meany = 0
    covar = [[dxx,dxy],[dxy,dyy]]
    #print(covar)
    igx1, igy1 = np.random.multivariate_normal([meanx,meany], covar, 500).T
    rot = random()*2*pi
    rot = 0
    cs, sn = cos(rot) , sin(rot)    
    gx1, gy1 = igx1*cs-igy1*sn, igx1*sn+igy1*cs
    cs, sn = cos(radians(120)) , sin(radians(120))
    gx2, gy2 = gx1*cs-gy1*sn, gx1*sn+gy1*cs
    gx3, gy3 = gx2*cs-gy2*sn, gx2*sn+gy2*cs
    if(i%int(0.1*ndata)== 0):
        plt.scatter(gx1,gy1,color='red')
        plt.scatter(gx2,gy2,color='green')
        plt.scatter(gx3,gy3,color='blue')
        plt.xlim(-10, 10)
        plt.ylim(-10, 10)
        plt.show()
     
    X.append([gx1.mean(),gx1.std(),gx2.mean(),gx2.std(),gx3.mean(),gx3.std()])
    y.append([meanx,meany,dxx,dxy,dyy])
print("Generated data")
end = time.time()
print("Time to generate data = ",end-start," s")
start = time.time()
X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    random_state=1)
regr = MLPRegressor(random_state=1, max_iter=50000).fit(X_train, y_train)
end = time.time()
print("Time to train data= ",end-start," s")
print("Data: ",X_test[:1])
print("Original: ",y_test[:1])
print("Predicted: ",regr.predict(X_test[:1]))
print(regr.score(X_test, y_test))
diff_meanx = 100.*abs((y_test[:-1][0]-regr.predict(X_test[:1]))/y_test[:-1][0])
print("Average error in mean x(%): ",diff_meanx.mean())